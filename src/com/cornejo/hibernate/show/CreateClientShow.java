package com.cornejo.hibernate.show;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.cornejo.hibernate.show.entity.Client;

public class CreateClientShow {

	public static void main(String[] args) {
		
		// Create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Client.class)
								.buildSessionFactory();
		
		// Create session

		try (factory) {
			Session session = factory.getCurrentSession();
			// --------- use the session object to save Java Object ---------

			// create a student object
			System.out.println("Creating new Client object...");
			Client tempClient1 = new Client("Ross", "Melendez", "rossimer@hotmail.com");
			Client tempClient2 = new Client("Beni", "Melendez", "beni_ya@hotmail.com");
			Client tempClient3 = new Client("Betty", "Melendez", "betty_bor@hotmail.com");

			// start a transaction
			session.beginTransaction();

			// save the student object
			System.out.println("Saving the Client");
			session.save(tempClient1);
			session.save(tempClient2);
			session.save(tempClient3);

			// commit transaction
			session.getTransaction().commit();

			System.out.println("Done!");


		} catch (Throwable ex) {

			System.out.println("Error to begin Hibernate" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

}
